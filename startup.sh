#!/bin/bash

echo 'Usage: ./startup.sh <input folder> <output file> <optional: recovery id>'
echo '<input folder> is the folder containing all captchas to solve'
echo '<output file> is the file to write classificaitions to'
echo '<recovery id> is the timestsamp of .save files made as the program runs as a back up'
echo '-->in a file a_b_c.save, the timestamp is b'
echo ''

#Check command-line args
if [[ "$#" -lt 2 ]]
then
    echo 'Please provide the following arguments: input (captcha) folder and output file name'
    exit 1
fi

#Source venv
echo 'setting up venv (will create venv if there is no existing one)'
if [[ -d ~/captcha_env ]]
then
    echo 'using existing venv'
else
    echo 'creating venv'
    mkdir ~/captcha_env
    python3 -m venv ~/captcha_env
fi
source ~/captcha_env/bin/activate

#Pull or update code and echo results
echo 'attempting to check for and apply updates (will timeout and continue on bad connection)'
if [[ -d .git ]]
then
    echo 'In a git repo, so pulling code...'
    git reset --hard
    git pull https://artur-hawkwing:NascNeamhSlan@bitbucket.org/jeffrey-siothrun/scalable-computing-project-2-pi.git
    exitstatus=$?
else
    echo 'Not in a a git repo...creating directory structure'
    mkdir ~/solver
    cd ~/solver
    
    if [[ -d scalable-computing-project-2-pi/  ]] #Looks like this script got moved
    then
        cd scalable-computing-project-2-pi/ 
        if [[ -d .git ]]
        then
            echo 'Found the missing git repo, so pulling code...'
            git reset --hard
            git pull https://artur-hawkwing:NascNeamhSlan@bitbucket.org/jeffrey-siothrun/scalable-computing-project-2-pi.git
            exitstatus=$?
        else
            cd ..
            rm -r scalable-computing-project-2-pi/ 
            git clone https://artur-hawkwing:NascNeamhSlan@bitbucket.org/jeffrey-siothrun/scalable-computing-project-2-pi.git
            exitstatus=$?
            cd scalable-computing-project-2-pi/ 
        fi
    else
        git clone https://artur-hawkwing:NascNeamhSlan@bitbucket.org/jeffrey-siothrun/scalable-computing-project-2-pi.git
        exitstatus=$?
        cd scalable-computing-project-2-pi/ 
    fi
fi
chmod u+x startup.sh

case $exitstatus in
    0)
        echo $exitstatus 'all updates pulled successfully'
        ;;
    *)
        echo $exitstatus 'failed to pull updates, attempting to continuing with existing code'
        ;;
esac

#Install or update packages
echo 'attempting to update / install needed packages (will timeout and continue on bad connection)'
python3 install.py

#Classify captchas
echo 'Running classification'
case $# in
    2)
        python3 classify.py --model-name model/model --captcha-dir $1 --output $2 --symbols model/symbols.txt --captcha-len 6 --processes 4
        exitstatus=$?
        ;;
    3)
        python3 classify.py --model-name model/model --captcha-dir $1 --output $2 --symbols model/symbols.txt --captcha-len 6 --processes 4 --continue-from $3
        exitstatus=$?
        ;;
esac

#Echo final results
if [[ $exitstatus -eq 0 ]]
then
    echo $exitstatus 'classification ended successfully'
else
    echo $exitstatus 'classification failed'
fi
