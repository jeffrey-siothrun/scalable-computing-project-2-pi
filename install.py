'''
Referemces:
    For installing a package from Python code:
    https://stackoverflow.com/questions/12332975/installing-python-module-within-code
'''

import sys
import subprocess

def install(package):
    error = subprocess.check_call([sys.executable, "-m", "pip", "install", package])
    if error != 0:
        raise ValueError('could not install package')

with open('requirements.txt', 'r') as inp:
    reqs = inp.readlines()

failed = 0
failed_packages = []
for req in reqs:
    if not req in sys.modules:
        try:
            install(req)
        except:
            failed += 1
            failed_packages.append(req)

if failed > 0:
    print(str(failed) + ' packages not installed, this may cause classification to fail')
    print('failed packages: ' + str(failed_packages))
    exit(1)
else:
    print('all ' + str(len(reqs)) + ' packages installed correctly')
    exit(0)