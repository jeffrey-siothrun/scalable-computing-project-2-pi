# Captcha Solver
## Jeffrey Sardina

This code predicts the text inside a captcha. The entire system is automated.

To run it, use the command:
    ./startup.sh captcha-dir output-file

Using the default file configuration, this is:
    ./startup.sh in/base/ output.txt

Please note that, if you just pulled the code from Git, you may have to run
    chmod u+x ./startup.sh
in order to be able to run the solver.
